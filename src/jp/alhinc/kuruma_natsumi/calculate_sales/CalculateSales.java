package jp.alhinc.kuruma_natsumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;


public class CalculateSales {
	public static void main(String[ ] args) {

		//エラー処理
		//コマンドライン引数が渡されていない場合
		//コマンドライン引数が２つ以上の場合
		if(args.length != 1) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		}

		BufferedReader br = null;

		HashMap<String, String> branchMap = new HashMap<String, String>();
		HashMap<String,Long> salesMap = new HashMap<String,Long>();

		//ファイルの読み込みメソッド処理
		if(!readFile(args[0],"branch.lst", "支店定義","^\\d{3}$",branchMap,salesMap)) {
			return;
		}



		//引数のファイルの一覧を取得する
		File dir = new File(args[0]);
		File[] fileName = dir.listFiles();
		//引数内の指定ファイルでリストを作成する
		ArrayList<File> fileList = new ArrayList<File>();

		//引数のファイルの数処理を繰り返す
		for(int i=0; i<fileName.length; ++i) {

			//エラー処理
			//8桁かつrcd拡張子にマッチしない場合でかつ
			//最大値でないフォルダーが含まれている場合
			if(fileName[i].getName().matches("^[0-9]{8}.*rcd$") && fileName[i].isFile()){
				//８桁、rcd拡張子、フォルダー以外に当てはまるファイルをfileListに入れる
				fileList.add(fileName[i]);
			}
		}


		//指定の売上ファイルを比較しながら繰り返す
		for(int j = 0; j < fileList.size()-1; j++) {

			//エラー処理
			//売上ファイル連番チェック　歯抜けになっている場合
			//ファイルをストリング型に直す
			String str0 = fileList.get(j).getName();
			//0-8を抜き取る
			String number = str0.substring(0, 8);
			//int型に型変換する
			int rcd1= Integer.parseInt(number);

			//比較対象となるjの二番目の数を変換する
			String str1 = fileList.get(j+1).getName();
			String number1 = str1.substring(0, 8);
			int rcd2= Integer.parseInt(number1);

			//00000001.rcdに１を足したものと00000002.rcdの数が同一か確かめる
			if((rcd2)!= (rcd1)+1) {
				System.out.println("売上ファイル名が連番になっていません");
				return;
			}
		}

		for(int j = 0; j < fileList.size(); j++) {
			try {
					FileReader fr = new FileReader(fileList.get(j));
					br = new BufferedReader(fr);

					//売上ファイルから支店コードと売上額をリストに入れる
					String lines;
					ArrayList<String> fileContents = new ArrayList<String>();

					while((lines = br.readLine()) != null) {
						fileContents.add(lines);
					}
					//エラー処理
					//売上ファイルの支店コードが支店定義ファイルに存在しない場合
					//集計を出す売上ファイルの支店コードは支店定義ファイルの中にあるものと同一である必要がある
					if(!branchMap.containsKey(fileContents.get(0))) {
						System.out.println(fileList.get(j).getName()+"の支店コードが不正です");
						return;
					}

					//エラー処理
					//売上ファイルの行数が1行以下の場合
					//売上ファイルの行数が３行以上の場合
					if(fileContents.size() != 2) {
						System.out.println(fileList.get(j).getName()+"のフォーマットが不正です");
						return;
					}



					//エラー処理
					//売上金額に平仮名が含まれている場合
					//売上金額にカタカナが含まれている場合
					//売上金額に漢字が含まれている場合
					//売上金額に記号が含まれている場合
					if(!fileContents.get(1).matches("[0-9]{0,10}")){
						System.out.println("予期せぬエラーが発生しました");
						return;
					}

					//売上ファイルの合計を取り出す
					String str = fileContents.get(1);
					//合計をLong型に型変換する
					Long lon = Long.parseLong(str);

					//売上ファイルの合計を算出する
					long amount = salesMap.get(fileContents.get(0))+lon;
					salesMap.put(fileContents.get(0), amount);

					//エラー処理
					//支店別集計合計金額が10桁超えた
					long lon10over = amount;
						//LongをString型に型変換する
						String str10over = String.valueOf(lon10over);
					if(!str10over.matches("[0-9]{0,10}")){
						System.out.println("合計金額が10桁を超えました");
						return;
					}

			}catch(IOException e){
				System.out.println("予期せぬエラーが発生しました。");
			}finally {
				if(br != null) {
					try {
						br.close();
					}catch(IOException e){
						System.out.println("予期せぬエラーが発生しました");
					}
				}
			}
		}

		//出力メソッド処理
		if(!output(args[0],"branch.out", branchMap,salesMap)) {
			return;
		}
	}


	//出力メソッド
	public static boolean output(String dirPath,String fileName,HashMap<String, String> difinitionMap,HashMap<String, Long> aggregateMap) {
		BufferedWriter bw = null;

		try {
			//fileNameに出力する
			File file = new File(dirPath,fileName);
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);
			PrintWriter pw = new PrintWriter(bw);

			//合計金額を出力する
			for(String code : aggregateMap.keySet()){
				pw.println(code + "," + difinitionMap.get(code) + "," + aggregateMap.get(code));
			}
			pw.close();
		}catch(IOException e){
			System.out.println("予期せぬエラーが発生しました");
		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e){
					System.out.println("予期せぬエラーが発生しました");
				}
			}
		}
			return true;
	}

	//読み込みメソッド
	public static boolean readFile(String dirPath,String fileName,String fileString,String codeDesignation,HashMap<String, String> difinitionMap,HashMap<String, Long> aggregateMap) {

		BufferedReader br = null;
		try {
			File file = new File(dirPath,fileName);

			//エラー処理
			//支店定義ファイルが存在しない場合
			if(!file.exists()) {
				System.out.println(fileString +"ファイルが存在しません");
				return false;
			}
			FileReader fr = new FileReader(file);
			br = new BufferedReader(fr);

			String line;

			//支店定義ファイルの保持
			while((line = br.readLine()) != null) {

				//支店定義ファイルを支店コード，支店名に分ける
				String[] difinition = line.split(",");

				//エラー処理
				//支店定義ファイル１行の要素数が１つ以下の場合
				//支店定義ファイル１行の要素数が３つ以上の場合
				if(difinition.length != 2) {
					System.out.println(fileString + "ファイルのフォーマットが不正です");
					return false;
				}

				//エラー処理
				//支店コードに数値以外の値
				//支店コードにアルファベットが含まれている場合
				//支店コードにひらがなが含まれている場合
				//支店コードにカタカナが含まれている場合
				//支店コードに漢字が含まれている場合
				//支店コードに記号が含まれている場合
				//支店コードの桁数が２桁以下の場合
				//支店コードの桁数が４桁以上の場合
				if(!difinition[0].matches(codeDesignation)){
					System.out.println(fileString + "ファイルのフォーマットが不正です");
					return false;
				}

				//支店定義ファイルのマップに支店コード，支店名を入れる
				difinitionMap.put(difinition[0],difinition[1]);

				//売上ファイルのマップに支店コード，売上額を入れる
				aggregateMap.put(difinition[0],0L);
			}

		}catch(IOException e) {
			System.out.println("予期せぬエラーが発生しました。");
		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e){
				}
			}
		}
		return true;
	}
}

















